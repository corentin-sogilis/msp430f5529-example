/**
 * SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS
 *
 * main.c : Basic program to blink the leds (P4.7 and P1.0) of
 *          the MSP430F5529
 * Author : corentin <corentin@sogilis.com>,
 *          alex <alexendre@sogilis.com,
 *          from various Internet sources
 *
 * SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS
 */

#include <msp430.h>

int main(int argc, char *argv[])
{
    /* Hold the watchdog */
    WDTCTL = WDTPW + WDTHOLD;

    /* Set P1.0 and P4.7 directions to output */
    P1DIR |= 0x01;
    P4DIR |= 0x80;

    /* Set P1.0 output low and P4.7 output high */
    P1OUT &= 0xFE;
    P4OUT |= 0x80;

    while (1) {
        /* Wait for 400000 cycles */
        __delay_cycles(400000);

        /* Toggle outputs */
        P1OUT ^= 0x01;
        P4OUT ^= 0x80;
    }
}
