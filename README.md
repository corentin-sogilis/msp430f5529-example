# Project msp430f5529 example code

## Content

File | Description
-----|------------
main.c     | Basic led blink example
Makefile   | Build commands using the Docker image sogicoco/msp430f5529-devkit
script.gdb | Initial command to run in GDB to connect to the gdb instance and load the firmware binary

## Build project

The following command will call the docker image, download it
if it is not already present in the docker cache, and execute
msp430-elf-gcc by running the image as a container.

```
make
```

## Flash firmware on the msp430f5529

The following command will call the docker image, download it
if it is not already present in the docker cache, and execute
mspdebug by running the image as a container.

```
make flash
```

## Debug

In order to debug the program with gdb, it is first necessary
to execut gdbserver on the debuger through mspdebug.

```
make gdb-server
```

Then, when the server is running in our main tty,  we can run
msp430-elf-gdb in the docker container from another tty and
access a standard gdb shell.

```
make gdb
```

## What ? A Docker image ?

This project uses a docker image provided by [Sogilis](https://hub.docker.com/r/sogicoco/msp430f5529-devkit/) to set up instantly a complete development environment for the msp430f5529 launchpad.

The docker image contains all the msp430 toolchain, from the compiler msp430-elf-gcc to the latest mspdebug version. Hence, it is only necessary to install docker on a gnu/linux machine to get this environment available and to be able to compile this project.

The Dockerfile to build the image is hosted [there](https://gitlab.com/corentin-sogilis/msp430f5529-devkit).
