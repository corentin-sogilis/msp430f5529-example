# SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS
#
# Makefile : Build basic blink example for msp430f5529
# Author : Corentin <corentin@sogilis.com>, Alex <alexandre@sogilis.com>
#
# SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS

DOCKER   = docker run --rm --privileged --name devkit-msp430f5529-instance \
                  -v $(shell pwd):/opt/workspace -it \
                  sogicoco/msp430f5529-devkit
DOCKERX  = docker exec -it devkit-msp430f5529-instance
CC       = $(DOCKER) msp430-elf-gcc -mmcu=msp430f5529 -I /opt/include/ -I ./MSP430F5xx_6xx -L /opt/include/
MSPDEBUG = $(DOCKER) mspdebug tilib
GDB      = $(DOCKERX) msp430-elf-gdb
ELF      = firmware.elf
CFLAGS   = -g

DRV_LIB = MSP430F5xx_6xx/adc10_a.c \
          MSP430F5xx_6xx/adc12_a.c \
          MSP430F5xx_6xx/aes.c \
          MSP430F5xx_6xx/battbak.c \
          MSP430F5xx_6xx/comp_b.c \
          MSP430F5xx_6xx/crc.c \
          MSP430F5xx_6xx/ctsd16.c \
          MSP430F5xx_6xx/dac12_a.c \
          MSP430F5xx_6xx/dma.c \
          MSP430F5xx_6xx/eusci_a_spi.c \
          MSP430F5xx_6xx/eusci_a_uart.c \
          MSP430F5xx_6xx/eusci_b_i2c.c \
          MSP430F5xx_6xx/eusci_b_spi.c \
          MSP430F5xx_6xx/flashctl.c \
          MSP430F5xx_6xx/gpio.c \
          MSP430F5xx_6xx/lcd_b.c \
          MSP430F5xx_6xx/ldopwr.c \
          MSP430F5xx_6xx/mpy32.c \
          MSP430F5xx_6xx/oa.c \
          MSP430F5xx_6xx/pmap.c \
          MSP430F5xx_6xx/pmm.c \
          MSP430F5xx_6xx/ram.c \
          MSP430F5xx_6xx/ref.c \
          MSP430F5xx_6xx/rtc_a.c \
          MSP430F5xx_6xx/rtc_b.c \
          MSP430F5xx_6xx/rtc_c.c \
          MSP430F5xx_6xx/sd24_b.c \
          MSP430F5xx_6xx/sfr.c \
          MSP430F5xx_6xx/sysctl.c \
          MSP430F5xx_6xx/tec.c \
          MSP430F5xx_6xx/timer_a.c \
          MSP430F5xx_6xx/timer_b.c \
          MSP430F5xx_6xx/timer_d.c \
          MSP430F5xx_6xx/tlv.c \
          MSP430F5xx_6xx/ucs.c \
          MSP430F5xx_6xx/usci_a_spi.c \
          MSP430F5xx_6xx/usci_a_uart.c \
          MSP430F5xx_6xx/usci_b_i2c.c \
          MSP430F5xx_6xx/usci_b_spi.c \
          MSP430F5xx_6xx/wdt_a.c

.PHONY : all main flash debug clean gdb-server gdb

all : main

main : main.c usci_a_uart_ex2_addressBitMultiprocessorRX.c $(DRV_LIB)
	$(CC) $(CFLAGS) $^ -o $(ELF)

flash : $(ELF) main
	$(MSPDEBUG) 'prog $<'

fw-update :
	$(MSPDEBUG) --allow-fw-update

gdb-server : main
	$(MSPDEBUG) 'gdb'

cmd :
	$(MSPDEBUG)

gdb :
	$(GDB) --eval-command="target remote localhost:2000" $(ELF)

clean :
	rm -rf $(ELF) *~
